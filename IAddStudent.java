import java.util.ArrayList;

public interface IAddStudent {
	public String getFirstName();	
	public String getLastName();	
	public String getAM();
	public void closeWindow();
	public ArrayList<Student>getDB();
}
