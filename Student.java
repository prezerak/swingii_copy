
public class Student {
	private String firstName; 
	private String lastName; 
	private String studentId;	
	
	public Student(String am, String first, String last) {
		this.studentId = am;
		this.firstName = first;
		this.lastName = last;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	
}
