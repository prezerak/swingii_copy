import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JDialog;

public class AddDlgListener implements ActionListener {

	private ArrayList<Student> database;

	public AddDlgListener(ArrayList<Student> database) {
		this.database = database;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		AddDlg dialog = new AddDlg(database);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);
	}

}
