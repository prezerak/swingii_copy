import java.awt.event.*;
import javax.swing.*;


public class AboutListener implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent e) {						
		var dialog = new JDialog();
		dialog.setTitle("About Dialog");
		dialog.setDefaultCloseOperation(
				JDialog.DISPOSE_ON_CLOSE);
		dialog.setModal(true);
		dialog.setBounds(60,60, 100, 220);
		dialog.setLayout(null);
		var label = new JLabel("!@$@!$@$#");
		label.setBounds(5, 10, 140, 30);
		dialog.add(label);
		var closeButton = new JButton("Close");
		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(false);
			}
		});
		closeButton.setBounds(40, 100, 90,30);
		dialog.add(closeButton);
		dialog.setVisible(true);		
	}

}
