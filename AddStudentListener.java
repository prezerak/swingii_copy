import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddStudentListener implements ActionListener {

	private IAddStudent myDialog;
	
	public AddStudentListener(IAddStudent dlg) {
		this.myDialog = dlg;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String first = this.myDialog.getFirstName();
		String last = this.myDialog.getLastName();
		String am = this.myDialog.getAM(); 
		
		System.out.println(first);
		System.out.println(last);
		System.out.println(am);
		
		Student s = new Student(am, first, last);
		this.myDialog.getDB().add(s);		
		this.myDialog.closeWindow();
	}

}
