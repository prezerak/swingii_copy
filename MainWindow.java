import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MainWindow extends JFrame {
	
	ArrayList<Student> database = 
			new ArrayList<Student>();

	public MainWindow(String title, int x, int y, int width, int height) {

		this.setTitle(title);
		this.setBounds(x, y, width, height);

		this.setLayout(null);
		var menuBar = new JMenuBar();

		this.setJMenuBar(menuBar);

		JMenu file = new JMenu("File");
		JMenu help = new JMenu("Help");
		menuBar.add(file);
		menuBar.add(help);
		JMenuItem open = new JMenuItem("Add Student");
		AddDlg dialog = new AddDlg(database);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		open.addActionListener(new AddDlgListener(this.database));
		open.setActionCommand("Open");
		JMenuItem search = new JMenuItem("Search");
		search.addActionListener(new SearchListener(this.database));
		JMenuItem exit = new JMenuItem("Exit");
		JMenuItem delete = new JMenuItem("Delete student");
		file.add(open);
		file.add(search);
		file.add(exit);
		file.add(delete);
		JMenuItem about = new JMenuItem("About");
		about.addActionListener(new AboutListener());
		about.setActionCommand("About");
		help.add(about);
	}
}
