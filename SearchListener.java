import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Optional;

public class SearchListener implements ActionListener {

	private ArrayList<Student> database;

	public SearchListener(ArrayList<Student> database) {
		this.database = database;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Optional<Student> s1 = this.database.stream()
					.filter(s -> 
							s.getStudentId().compareTo("102")==0)
					.findFirst();
		
	if (s1.isEmpty()) {		 
			System.out.println("Not found !!!");
	}
	else {
		Student foo = s1.get();
		System.out.println(foo.getLastName());
	}
}

}
