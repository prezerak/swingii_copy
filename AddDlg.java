import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class AddDlg extends JDialog 
	implements IAddStudent {
	private JTextField fNameTxt;
	private JTextField lNameTxt;
	private JTextField amTxt;
	private ArrayList<Student> database;
	

	/**
	 * Create the dialog.
	 */
	public AddDlg(ArrayList<Student> database) {
		this.database = database;
		var dlg = this;
		setTitle("Add Student Form");
		setBounds(100, 100, 450, 384);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.
					addActionListener(
							new AddStudentListener(dlg));
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dlg.setVisible(false);
					}});
				buttonPane.add(cancelButton);
			}
		}
		{
			JPanel panel = new JPanel();
			getContentPane().add(panel, BorderLayout.CENTER);
			panel.setLayout(null);
			
			JLabel lblNewLabel = new JLabel("First name:");
			lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel.setBounds(10, 95, 90, 27);
			panel.add(lblNewLabel);
			
			JLabel lblLastName = new JLabel("Last name:");
			lblLastName.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblLastName.setBounds(10, 133, 90, 27);
			panel.add(lblLastName);
			
			JLabel lblAm = new JLabel("A.M.");
			lblAm.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblAm.setBounds(10, 181, 90, 27);
			panel.add(lblAm);
			
			fNameTxt = new JTextField();
			fNameTxt.setBounds(135, 100, 96, 20);
			panel.add(fNameTxt);
			fNameTxt.setColumns(10);
			
			lNameTxt = new JTextField();
			lNameTxt.setColumns(10);
			lNameTxt.setBounds(135, 138, 96, 20);
			panel.add(lNameTxt);
			
			amTxt = new JTextField();
			amTxt.setColumns(10);
			amTxt.setBounds(135, 186, 96, 20);
			panel.add(amTxt);
		}
	}
	
	public String getFirstName() {
		return this.fNameTxt.getText();
	}
	
	public String getLastName() {
		return this.lNameTxt.getText();
	}
	
	public String getAM() {
		return this.amTxt.getText();
	}

	@Override
	public void closeWindow() {
		this.setVisible(false);		
	}
	
	public ArrayList<Student>getDB() {
		return this.database;
	}
}
